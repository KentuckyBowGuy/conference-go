import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}+{state}&per_page=1"
    headers = {"Authorization": PEXELS_API_KEY}

    response = requests.get(url, headers=headers)
    pic = json.loads(response.text)

    return pic["photos"][0]["url"]


def get_weather_data(
    city,
    state,
):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    geo = json.loads(response.text)
    lat = geo[0]["lat"]
    lon = geo[0]["lon"]

    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    response = requests.get(url)
    weather_data = json.loads(response.text)
    weather = {
        "temp": weather_data["main"]["temp"],
        "description": weather_data["weather"][0]["description"],
    }

    return (weather,)
